require("@nomiclabs/hardhat-waffle");
require('dotenv').config();

task("accounts", "Prints the list of accounts", async(taskArgs, hre) =>{
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts){
    console.log(account.address);
  }
});
const sepolia_url = process.env.sepolia_url;
const private_key = process.env.private_key;

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.24",
  settings: {
    optimizer: {
      enabled: true,
      runs: 200    
    }
  },
  networks:{
    sepolia: {
      url: sepolia_url,
      accounts: [private_key],
    },
  }
};
