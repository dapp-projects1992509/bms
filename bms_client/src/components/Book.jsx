const Book = () => (
  <svg
    width="658"
    height="867"
    viewBox="0 0 658 867"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g filter="url(#filter0_dd_502_2)">
      <rect
        x="14"
        y="14"
        width="618"
        height="827"
        rx="20"
        fill="url(#paint0_linear_502_2)"
      />
    </g>
    <g opacity="0.4" filter="url(#filter1_f_502_2)">
      <path
        d="M14 33C14 33 14.9379 27.4289 16.5 24.5C18.0621 21.5711 22 18 22 18V837C22 837 18.0621 833.929 16.5 831C14.9379 828.071 14 822 14 822V33Z"
        fill="url(#paint1_linear_502_2)"
      />
    </g>
    <g opacity="0.4" filter="url(#filter2_f_502_2)">
      <path
        d="M632 33C632 33 631.062 27.4289 629.5 24.5C627.938 21.5711 624 18 624 18V837C624 837 627.938 833.929 629.5 831C631.062 828.071 632 822 632 822V33Z"
        fill="url(#paint2_linear_502_2)"
      />
    </g>
    <g opacity="0.55" filter="url(#filter3_f_502_2)">
      <rect
        x="41"
        y="14"
        width="31"
        height="827"
        fill="url(#paint3_linear_502_2)"
      />
    </g>
    <defs>
      <filter
        id="filter0_dd_502_2"
        x="0"
        y="0"
        width="658"
        height="867"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          result="hardAlpha"
        />
        <feMorphology
          radius="5"
          operator="dilate"
          in="SourceAlpha"
          result="effect1_dropShadow_502_2"
        />
        <feOffset dx="6" dy="6" />
        <feGaussianBlur stdDeviation="7.5" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
        />
        <feBlend
          mode="normal"
          in2="BackgroundImageFix"
          result="effect1_dropShadow_502_2"
        />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          result="hardAlpha"
        />
        <feOffset dx="6" dy="6" />
        <feGaussianBlur stdDeviation="5" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"
        />
        <feBlend
          mode="normal"
          in2="effect1_dropShadow_502_2"
          result="effect2_dropShadow_502_2"
        />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="effect2_dropShadow_502_2"
          result="shape"
        />
      </filter>
      <filter
        id="filter1_f_502_2"
        x="13.5"
        y="17.5"
        width="9"
        height="820"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="BackgroundImageFix"
          result="shape"
        />
        <feGaussianBlur
          stdDeviation="0.25"
          result="effect1_foregroundBlur_502_2"
        />
      </filter>
      <filter
        id="filter2_f_502_2"
        x="623.5"
        y="17.5"
        width="9"
        height="820"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="BackgroundImageFix"
          result="shape"
        />
        <feGaussianBlur
          stdDeviation="0.25"
          result="effect1_foregroundBlur_502_2"
        />
      </filter>
      <filter
        id="filter3_f_502_2"
        x="40.9"
        y="13.9"
        width="31.2"
        height="827.2"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="BackgroundImageFix"
          result="shape"
        />
        <feGaussianBlur
          stdDeviation="0.05"
          result="effect1_foregroundBlur_502_2"
        />
      </filter>
      <linearGradient
        id="paint0_linear_502_2"
        x1="632"
        y1="512.519"
        x2="13.9112"
        y2="263.48"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="white" />
        <stop offset="1" stopColor="#DFDFDF" />
      </linearGradient>
      <linearGradient
        id="paint1_linear_502_2"
        x1="22"
        y1="385.1"
        x2="14"
        y2="385.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="white" stopOpacity="0" />
        <stop offset="0.614583" stopColor="white" />
      </linearGradient>
      <linearGradient
        id="paint2_linear_502_2"
        x1="624"
        y1="385.1"
        x2="632"
        y2="385.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="white" stopOpacity="0" />
        <stop offset="0.614583" stopColor="white" />
      </linearGradient>
      <linearGradient
        id="paint3_linear_502_2"
        x1="72"
        y1="383.058"
        x2="41"
        y2="383.058"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="white" stopOpacity="0" />
        <stop offset="0.354167" stopColor="#E0E0E0" stopOpacity="0.6" />
        <stop offset="0.640625" stopColor="#E0E0E0" stopOpacity="0.6" />
        <stop offset="1" stopOpacity="0" />
      </linearGradient>
    </defs>
  </svg>
);

export default Book;
