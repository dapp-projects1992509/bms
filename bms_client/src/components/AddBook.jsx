import { ethers } from "ethers";
import "./AddBook.css";

const AddBook = ({ state }) => {
  const addBook = async (event) => {
    event.preventDefault();

    const { contract } = state;
    const name = document.querySelector("#name").value;
    const year = parseInt(document.querySelector("#year").value);
    const author = String(document.querySelector("#author").value);
    const isCompleted = document.querySelector("#isCompleted").value == "true";

    console.log(name, year, author, isCompleted);

    try {
      //define transaction which waits for the transaction to be mined
      const transaction = await contract.addBook(
        name,
        year,
        author,
        isCompleted
      );
      console.log("waiting for transaction");

      await transaction.wait();
      alert("Transaction is Successful!");
      console.log("Transaction is Done!");
      window.location.reload();
    } catch (error) {
      console.log("Error adding book", error);
      alert("Error adding book");
    }
  };

  return (
    <>
      <h2 className="add-book-title">Add a book</h2>
      <form onSubmit={addBook} className="add-form">
        <input id="name" placeholder="Book Name" className="add-input" />
        <input id="year" placeholder="Publication Year" className="add-input" />
        <input id="author" placeholder="Author" className="add-input" />
        <span>Complete Reading?</span>
        <select id="isCompleted" className="add-select">
          <option value="true" className="add-selection">
            Yes
          </option>
          <option value="false" className="add-selection">
            No
          </option>
        </select>
        <br />
        <button className="add-button">Add to List</button>
      </form>
    </>
  );
};
export default AddBook;
