import { useEffect, useState } from "react";
import "./AddedList.css";
import Book from "./Book";
import Tick from "./Tick";
import X from "./X";

const AddedList = ({ state }) => {
  const [lists, setLists] = useState([]);
  const [booksRead, setBooksRead] = useState(0);
  const { contract } = state;
  console.log(contract);

  useEffect(() => {
    const listDetails = async () => {
      try {
        const fetchedLists = await contract.getFinishedBook();
        console.log(fetchedLists);
        setLists(fetchedLists);
        const numBooksRead = fetchedLists.filter((book) => book.isCompleted).length;
        setBooksRead(numBooksRead);
      } catch (error) {
        console.log("Error fetching list", error);
      }
    };
    contract && listDetails();
  }, [contract]);
  return (
    <>
        {/* in order to display the list of books entered */}
        {lists.map((book) => {
          return (
            <>
              <div className="book-design" key={book.id}>
                <Book />

                <div className="more-book-details">
                  <div className="book-id">
                    <p className="">{book.id.toString()}</p>
                  </div>
                  <div className="book-bool-read">
                    {book.isCompleted.toString() === "true" ? <Tick /> : <X />}
                  </div>
                </div>

                <div className="book-content">
                  <h3>{book.name.toString()}</h3>
                  <p className="author">~ {book.author.toString()}</p>
                </div>
                <p className="year">Published {book.year.toString()}</p>
              </div>
            </>
          );
        })}
        <div style={{ textAlign: "center", marginTop: "20px" }}>
    <p>Total Books Read: {booksRead}</p>
      </div>
    </>
  );
  
};
export default AddedList;
