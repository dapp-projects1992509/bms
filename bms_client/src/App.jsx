import { ethers } from "ethers"; //import ethers library
import { useState, useEffect } from "react";

import abi from "./contractJson/Booklist.json";
import AddBook from "./components/AddBook";
import AddedList from "./components/AddedList";

function App() {
  const [state, setState] = useState({
    provider: null,
    signer: null,
    contract: null,
  });

  const [account, setAccount] = useState("Not Connected");

  useEffect(() => {
    const template = async () => {
      const contractAddress = "0x378647d52e5b2dd54F148A73C1D882EC3420D1c0";
      const contractABI = abi.abi;

      // console.log(abi, contractAddress)
      //Metamask Connection
      //1. for transaction on sepolia testnets
      //2. consists of alchemy api which actually help in connecting to the blockchain network

      if (!window.ethereum) {
        alert("Install Metamask");
        return;
      }

      const { ethereum } = window;
      //3. Define provider and signer that will help connect with the blockchain
      //will be used to read from the blockchain
      const provider = new ethers.providers.Web3Provider(ethereum);

      //Define signer that will help in transaction to change the blockchain state
      // will be used to write to the blockchain

      const signer = provider.getSigner();

      //4. Create the instance of the contract communicate wiht the smart contract
      const contract = new ethers.Contract(
        contractAddress,
        contractABI,
        signer
      );
      console.log(contract);
      //invoke metamask wallet
      const account = await ethereum.request({
        method: "eth_requestAccounts",
      });

      window.ethereum.on("Account changed", () => {
        window.location.reload();
      });

      setState({ provider, signer, contract });
      setAccount(account);
    };
    template();
  }, []);

  return (
    <>
      <nav className="navbar">
        <div className="account">
          {account ? <div>Connected: {account[0]}</div> : "Not Connected"}
        </div>
      </nav>
      <div className="mainDiv">
        <img
          src="https://assets.softr-files.com/applications/64f7a139-4dc4-40ee-85ec-467615d9237d/assets/5d1d8a50-dd87-4d50-90bc-e3c4df41b4a6.svg"
          className="background_image"
          alt=".."
        />

        <div className="container">
          <AddBook state={state}></AddBook>
        </div>
        <h3 style={{ textAlign: "center", marginTop: "20px" }}>
          List of Books Added!
        </h3>
        <div className="lists">

        <AddedList state={state}/>
        </div>
      </div>

      <div className="circle" />
    </>
  );
}

export default App;
