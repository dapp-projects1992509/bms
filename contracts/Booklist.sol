// SPDX-License-Identifier: UNLICENSED 
pragma solidity >=0.7.6 <0.9.0;

contract Booklist{
    struct Book{
        uint id;
        string name;
        uint year;
        string author;
        bool isCompleted;
    }

    Book[] private BookList;
    mapping(uint256 => address) bookToOwner;

    event AddBook(address bookRecipient, uint BookId);
    event SetFinished(uint BookId, bool isCompleted);

    function addBook(string memory name, uint year, string memory author, bool isCompleted) external{
        uint BookId = BookList.length;
        BookList.push(Book(BookId, name, year, author, isCompleted));
        bookToOwner[BookId] = msg.sender;
        emit AddBook(msg.sender, BookId);
    }

    modifier onlyOwner(uint BookId){
        require(bookToOwner[BookId] == msg.sender, "You are not the owner of this book");
        _;
    }

    function setCompleted(uint BookId, bool completed) public onlyOwner(BookId){
        BookList[BookId].isCompleted = completed;
        emit SetFinished(BookId, completed);
    }

    function getFinishedBook() external view returns(Book[] memory){
        return getBookList(true);
    }

    function getUnfinishedBook() external view returns(Book[] memory){
        return getBookList(false);
    }

    function getAllBookList() private view returns (Book[] memory) {
    uint counter = 0;

    for (uint index = 0; index < BookList.length; index++) {
        if (bookToOwner[index] == msg.sender) {
            counter++;
        }
    }

    if (counter == 0) {
        return new Book[](0) ;  // Empty array
    }

    Book[] memory result = new Book[](counter);

    uint resultCounter = 0;

    for (uint index = 0; index < BookList.length; index++) {
        if (bookToOwner[index] == msg.sender) {
            result[resultCounter++] = BookList[index];
        }
    }       
    return result;
}

    function getAllBooks() external view returns (Book[] memory) {
        return getAllBookList();
    }

    
    function getBookList(bool finished) private view returns (Book[] memory){
        uint counter = 0;

        for (uint index = 0; index < BookList.length; index++){
            if (bookToOwner[index] == msg.sender && BookList[index].isCompleted == finished){
                counter++;
            }
        }

        if (counter == 0){
            return new Book[](0);  //Empty array
        }

        Book[] memory result = new Book[](counter);

        uint resultCounter = 0;

        for (uint index = 0; index < BookList.length; index++){
            if (bookToOwner[index] == msg.sender && BookList[index].isCompleted == finished){
                result[resultCounter++] = BookList[index];
            }
        }       
        return result;
    }
}